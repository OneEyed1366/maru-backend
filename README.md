# Maru backend server

## Stack

- Yarn
- Express
- Docker & Docker-Compose
- MongoDB (Mongoose)

## How-To: Install

### Dev

```
yarn

docker-compose --env-file ./.env -p maru --file .deploy/docker-compose.dev.yml up --remove-orphans -d

node src/utils/scripts/generate-data.script.js
```

### Prod

```
docker-compose --env-file ./.env -p maru --file .deploy/docker-compose.prod.yml up --remove-orphans -d
```
