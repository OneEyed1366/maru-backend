const { Schema, model } = require('mongoose');
const { enums } = require('../utils');

const nameImageSchema = 'imageSchema';
const ImageSchema = new Schema(
  {
    title: {
      type: String,
      required: true,
    },
    src: {
      type: String,
      required: true,
    },
    mimeType: {
      type: String,
      required: true,
      enum: Object.values(enums.IMAGE_MIMETYPES_ENUM),
    },
    deletedAt: {
      type: Date,
      default: null,
    },
  },
  { timestamps: true }
);

ImageSchema.virtual('isDeleted').get(function () {
  switch (true) {
    case this.deletedAt !== null:
      return true;
    default:
      return false;
  }
});

module.exports = {
  name: nameImageSchema,
  Schema: model(nameImageSchema, ImageSchema),
};
