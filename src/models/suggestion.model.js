const { Schema, model } = require('mongoose');

const suggestionModelName = 'suggestionModel';
const SuggestionSchema = new Schema(
  {
    mission: {
      type: Schema.Types.ObjectId,
      ref: 'missionSchema',
      required: true,
    },
    value: {
      type: String,
      required: true,
    },
    isWasUsed: {
      type: Boolean,
      default: false,
    },
    cost: {
      type: Number,
      required: true,
    },
    field: {
      type: String,
      required: true,
    },
    deletedAt: {
      type: Date,
      default: null,
    },
  },
  { timestamps: true }
);

SuggestionSchema.virtual('isDeleted').get(function () {
  switch (true) {
    case this.deletedAt !== null:
      return true;
    default:
      return false;
  }
});

module.exports = {
  name: suggestionModelName,
  Schema: model(suggestionModelName, SuggestionSchema),
};
