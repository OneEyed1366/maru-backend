const { Schema, model } = require('mongoose');
const missions = require('./mission.model');

const answerModelName = 'answerModel';
const AnswerSchema = new Schema(
  {
    mission: {
      type: Schema.Types.ObjectId,
      ref: missions.name,
      select: false,
    },
    value: {
      type: String,
      required: true,
      unique: true,
    },
    cost: {
      type: Number,
      required: true,
    },
    deletedAt: {
      type: Date,
      default: null,
    },
  },
  { timestamps: true }
);

AnswerSchema.virtual('isDeleted').get(function () {
  switch (true) {
    case this.deletedAt !== null:
      return true;
    default:
      return false;
  }
});

module.exports = {
  name: answerModelName,
  Schema: model(answerModelName, AnswerSchema),
};
