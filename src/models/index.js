const answer = require('./answer.model');
const question = require('./question.model');
const bird = require('./bird.model');
const transaction = require('./transaction.model');
const images = require('./image.model');
const mission = require('./mission.model');
const suggestion = require('./suggestion.model');
const level = require('./level.model');

module.exports = {
  level,
  suggestion,
  mission,
  images,
  transaction,
  bird,
  question,
  answer,
};
