const { Schema, model } = require('mongoose');
const images = require('./image.model');
const mission = require('./mission.model');

const birdModelName = 'birdModel';
const BirdSchema = new Schema(
  {
    mission: {
      type: Schema.Types.ObjectId,
      ref: mission.name,
    },
    image: {
      type: Schema.Types.ObjectId,
      ref: images.name,
    },
    successfulImage: {
      type: Schema.Types.ObjectId,
      ref: images.name,
    },
    isSaved: {
      type: Boolean,
      default: false,
    },
    isDead: {
      type: Boolean,
      default: false,
    },
    name: {
      type: String,
      required: true,
    },
    desc: {
      type: String,
    },
    voice: {
      type: String,
    },
  },
  { timestamps: true }
);

BirdSchema.virtual('isDeleted').get(function () {
  switch (true) {
    case this.deletedAt !== null:
      return true;
    default:
      return false;
  }
});

module.exports = {
  name: birdModelName,
  Schema: model(birdModelName, BirdSchema),
};
