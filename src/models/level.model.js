const { Schema, model } = require('mongoose');

const levelModelName = 'levelModel';
const LevelSchema = new Schema(
  {
    mission: {
      type: Schema.Types.ObjectId,
      ref: 'missionSchema',
    },
    value: {
      type: Number,
      required: true,
      unique: true,
    },
    deletedAt: {
      type: Date,
      default: null,
    },
  },
  { timestamps: true }
);

LevelSchema.virtual('isDeleted').get(function () {
  switch (true) {
    case this.deletedAt !== null:
      return true;
    default:
      return false;
  }
});

module.exports = {
  name: levelModelName,
  Schema: model(levelModelName, LevelSchema),
};
