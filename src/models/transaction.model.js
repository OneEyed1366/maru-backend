const { Schema, model } = require('mongoose');
const { TRANSACTION_TYPES_ENUM } = require('../utils').enums;

const transactionModelName = 'transactionSchema';
const TransactionSchema = new Schema(
  {
    type: {
      type: String,
      required: true,
      enum: Object.values(TRANSACTION_TYPES_ENUM),
    },
    amount: {
      type: Number,
      required: true,
    },
    comment: {
      type: String,
      required: true,
    },
    deletedAt: {
      type: Date,
      default: null,
    },
  },
  { timestamps: true }
);

TransactionSchema.virtual('isDeleted').get(function () {
  switch (true) {
    case this.deletedAt !== null:
      return true;
    default:
      return false;
  }
});

module.exports = {
  name: transactionModelName,
  Schema: model(transactionModelName, TransactionSchema),
};
