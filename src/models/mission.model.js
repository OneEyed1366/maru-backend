const { Schema, model } = require('mongoose');
const { enums } = require('../utils');
const level = require('./level.model');
const suggestion = require('./suggestion.model');
const question = require('./question.model');

const missionModelName = 'missionSchema';
const MissionSchema = new Schema(
  {
    level: {
      type: Schema.Types.ObjectId,
      ref: level.name,
    },
    isActive: {
      type: Boolean,
      default: false,
    },
    isCompleted: {
      type: Boolean,
      default: false,
    },
    secretCode: {
      type: String,
      required: true,
    },
    alternativeMission: {
      type: Schema.Types.ObjectId,
      ref: missionModelName,
      default: null,
    },
    missionType: {
      type: String,
      enum: Object.values(enums.MISSION_TYPES_ENUM),
      default: enums.MISSION_TYPES_ENUM.normal,
    },
    bird: {
      type: Schema.Types.ObjectId,
      ref: 'birdModel',
    },
    question: {
      type: Schema.Types.ObjectId,
      ref: question.name,
    },
    answer: {
      type: Schema.Types.ObjectId,
      ref: 'answerModel',
    },
    suggestions: [
      {
        type: Schema.Types.ObjectId,
        ref: suggestion.name,
      },
    ],
    deletedAt: {
      type: Date,
      default: null,
    },
  },
  { timestamps: true }
);

MissionSchema.virtual('isDeleted').get(function () {
  switch (true) {
    case this.deletedAt !== null:
      return true;
    default:
      return false;
  }
});

module.exports = {
  name: missionModelName,
  Schema: model(missionModelName, MissionSchema),
};
