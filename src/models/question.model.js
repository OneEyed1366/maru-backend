const { Schema, model } = require('mongoose');
const images = require('./image.model');

const questionModelName = 'questionModel';
const QuestionSchema = new Schema(
  {
    desc: {
      type: String,
      required: true,
    },
    image: {
      type: Schema.Types.ObjectId,
      ref: images.name,
      required: true,
    },
    deletedAt: {
      type: Date,
      default: null,
    },
  },
  { timestamps: true }
);

QuestionSchema.virtual('isDeleted').get(function () {
  switch (true) {
    case this.deletedAt !== null:
      return true;
    default:
      return false;
  }
});

module.exports = {
  name: questionModelName,
  Schema: model(questionModelName, QuestionSchema),
};
