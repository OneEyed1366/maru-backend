const {
  BirdService,
  LevelService,
  MissionService,
  AnswerService,
  SuggestionService,
  QuestionService,
  TransactionService,
} = require('../../services');
const { images } = require('../../models');
const { readdirSync } = require('fs');
const { extname, relative, resolve } = require('path');

const publicPath = resolve('./public');
const birdsImagesPath = resolve('public/img/birds/');
const levelsImagesPath = resolve('public/img/levels/');

const dbInitData = require('../db.json');

require('../../../server');
const { enums, shuffle } = require('../index');
const { TRANSACTION_TYPES_ENUM } = require('../enums.util');

const dirtyMissions = [
  'Съесть две изюминки',
  'Посмотреть любой фильм Педро Альмодовара',
  'Выпить два глотка обычный (а не зеро!) кока-колы',
  'Съесть жёлтый (а не зелёный!) банан',
  'Выпить чашку свежего (а не вчерашнего!) чаю',
];
// eslint-disable-next-line no-unused-vars
async function clearImages() {
  await images.Schema.deleteMany();
}
async function generateBirdsImagesScript() {
  const _names = readdirSync(birdsImagesPath).filter(
    (value) =>
      enums.IMAGE_MIMETYPES_ENUM[extname(value).replace('.', '')] !== undefined
  );

  return Promise.all(
    _names.map((bird) => {
      const title = bird.replace(extname(bird), '');
      const _ext = extname(bird).replace('.', '');
      const src =
        '/' + relative(publicPath, `${birdsImagesPath}/${title}.${_ext}`);

      return images.Schema.findOneAndUpdate(
        { title },
        {
          src,
          title,
          mimeType: enums.IMAGE_MIMETYPES_ENUM[_ext],
        },
        { upsert: true, new: true, setDefaultsOnInsert: true }
      );
    })
  );
}

async function generateLevelsImagesScript() {
  const promises = [];
  let _names = readdirSync(levelsImagesPath).filter(
    (value) =>
      enums.IMAGE_MIMETYPES_ENUM[extname(value).replace('.', '')] !== undefined
  );

  for (let i = 1; i < 21; i += 1) {
    const level = _names.find((title) => title === `${i}.jpg`);
    const title = level.replace(extname(level), '');
    const _ext = extname(level).replace('.', '');
    const src =
      '/' + relative(publicPath, `${levelsImagesPath}/${title}.${_ext}`);

    promises.push(
      images.Schema.findOneAndUpdate(
        { title },
        {
          src,
          title,
          mimeType: enums.IMAGE_MIMETYPES_ENUM[_ext],
        },
        { upsert: true, new: true, setDefaultsOnInsert: true }
      )
    );
  }

  return Promise.all(promises);
}
async function generateBirdsScript() {
  const _birds = await generateBirdsImagesScript();

  await BirdService._model.Schema.deleteMany();

  return Promise.all(
    _birds.map((bird) =>
      BirdService.create({
        image: bird._id,
        successfulImage: bird._id,
        name: bird.title,
      })
    )
  );
}

(async () => {
  const _birds = await generateBirdsScript();
  const _levelImages = await generateLevelsImagesScript();

  await AnswerService._model.Schema.deleteMany();
  await QuestionService._model.Schema.deleteMany();
  await SuggestionService._model.Schema.deleteMany();
  await TransactionService._model.Schema.deleteMany();
  await LevelService._model.Schema.deleteMany();
  await MissionService._model.Schema.deleteMany();

  await TransactionService.create({
    amount: 3,
    comment: 'dbInit',
    type: TRANSACTION_TYPES_ENUM.ADD,
  });

  const values = Object.entries(dbInitData);

  for (const [key, { answer: answerData, ...data }] of values) {
    const bird = _birds[Number(key) - 1];
    const dirtyMission = shuffle(dirtyMissions)[0];

    const alternativeQuestion = await QuestionService.create({
      desc: `Непотребство: ${dirtyMission}`,
      image: _levelImages[Number(key) - 1],
    });

    const question = await QuestionService.create({
      desc: 'Что это за место?',
      image: _levelImages[Number(key) - 1],
    });

    const level = await LevelService.create({
      value: key,
    });

    const alternativeMission = await MissionService.create({
      level,
      bird,
      isActive: false,
      question: alternativeQuestion,
      secretCode: data.secretCode,
      missionType: enums.MISSION_TYPES_ENUM.dirty,
    });

    const mission = await MissionService.create({
      ...data,
      level,
      bird,
      question,
      alternativeMission,
      missionType: enums.MISSION_TYPES_ENUM.normal,
    });

    const answer = await AnswerService.create({
      mission,
      value: answerData.value,
      cost: answerData.cost,
    });

    const suggestions = await Promise.all(
      answerData.value.split(',').map((value, idx) =>
        SuggestionService.create({
          value,
          mission,
          field: idx === 0 ? 'street' : 'houseNumber',
          cost: idx === 0 ? 1 : 2,
        })
      )
    );

    mission.answer = answer;
    mission.suggestions = suggestions;
    mission.save();

    alternativeMission.answer = answer;
    alternativeMission.save();

    level.mission = mission;
    level.save();
  }

  console.log('sync complete');
  process.exit(0);
})();
