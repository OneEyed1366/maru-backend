const enums = require('./enums.util');
const factories = require('./factories');
const shuffle = require('./shuffle.util');

module.exports = {
  enums,
  factories,
  shuffle,
  constructRoutePath: (prefix, path) =>
    path ? `/${prefix}/${path}` : '/' + prefix,
};
