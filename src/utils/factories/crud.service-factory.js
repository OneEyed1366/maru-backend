module.exports = class CrudFactoryMethod {
  _model;
  _serviceName;

  constructor() {
    Object.defineProperties(this, {
      _baseErrorMsg: {
        writable: false,
        enumerable: false,
        value: `${this._serviceName} ->`,
      },
    });
  }

  async create(data) {
    try {
      return this._model.Schema.create(data);
    } catch (error) {
      throw new Error(`${this._baseErrorMsg} create -> ${error}`);
    }
  }

  async readById(id) {
    try {
      return this._model.Schema.findById(id);
    } catch (error) {
      throw new Error(`${this._baseErrorMsg} readById -> ${error}`);
    }
  }

  async updateById(id, data) {
    try {
      delete data._id;

      return this._model.Schema.findByIdAndUpdate(id, {
        $set: {
          ...data,
        },
      });
    } catch (error) {
      throw new Error(`${this._baseErrorMsg} updateById -> ${error}`);
    }
  }

  async deleteById(id) {
    try {
      return this._model.Schema.findByIdAndUpdate(id, {
        $set: {
          deletedAt: new Date(),
        },
      });
    } catch (error) {
      throw new Error(`${this._baseErrorMsg} deleteById -> ${error}`);
    }
  }
};
