module.exports = class CrudControllerFactoryMethod {
  _service;

  async create({ body }, res, next) {
    try {
      const { data } = body;

      res.send(this._service.create(data));
    } catch (e) {
      next(e);
    }
  }

  async readById({ params }, res, next) {
    try {
      const { id } = params;

      res.send(this._service.readById(id));
    } catch (e) {
      next(e);
    }
  }

  async updateById({ body, params }, res, next) {
    try {
      const { id } = params;
      const { data } = body;

      delete data._id;

      res.send(this._service.updateById(id));
    } catch (e) {
      next(e);
    }
  }

  async deleteById({ params }, res, next) {
    try {
      const { id } = params;

      res.send(this._service.deleteById(id));
    } catch (e) {
      next(e);
    }
  }
};
