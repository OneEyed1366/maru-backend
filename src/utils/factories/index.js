const CrudServiceFactoryMethod = require("./crud.service-factory");
const CrudControllerFactoryMethod = require("./crud.controller-factory");

module.exports = {
  CrudServiceFactoryMethod,
  CrudControllerFactoryMethod,
};
