module.exports = {
  TRANSACTION_TYPES_ENUM: Object.freeze({
    ADD: 'add',
    DECREASE: 'decrease',
  }),
  MISSION_TYPES_ENUM: Object.freeze({
    normal: 'NORMAL',
    dirty: 'DIRTY',
  }),
  IMAGE_MIMETYPES_ENUM: Object.freeze({
    gif: 'image/gif',
    jpeg: 'image/jpeg',
    jpg: 'image/jpeg',
    pjpeg: 'image/pjpeg',
    png: 'image/png',
    svg: 'image/svg+xml',
    tiff: 'image/tiff',
    wbmp: 'image/vnd.wap.wbmp',
    webp: 'image/webp',
  }),
};
