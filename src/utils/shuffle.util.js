/**
 * Функция для перемешивания массива элементов с помощью алгоритма Фишера-Йетса
 * @template T
 * @param {T[]} items Массив неизвестных элементов, который надо перемешать
 *
 * @returns {T[]} Перемешанный массив элементов
 * */
module.exports = function shuffle(items) {
  let result = items;

  for (let i = result.length - 1; i > 0; i -= 1) {
    const j = Math.floor(Math.random() * (i + 1));
    const temp = result[i];

    result[i] = result[j];
    result[j] = temp;
  }

  return result;
};
