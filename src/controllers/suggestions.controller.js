const { SuggestionService } = require('../services');
const { CrudControllerFactoryMethod } = require('../utils/factories');

class SuggestionsController extends CrudControllerFactoryMethod {
  _service = SuggestionService;

  constructor() {
    super();
  }

  async getSuggestionCost({ params }, res, next) {
    try {
      const { id: missionId } = params;

      res.send(String(await SuggestionService.getSuggestionCost(missionId)));
    } catch (e) {
      next(e);
    }
  }

  async buySuggestionWithMissionId({ body }, res, next) {
    try {
      const { missionId } = body;

      res.send(await SuggestionService.buySuggestionWithMissionId(missionId));
    } catch (e) {
      next(e);
    }
  }
}

module.exports = new SuggestionsController();
