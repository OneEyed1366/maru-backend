const { LevelService } = require('../services');

const { CrudControllerFactoryMethod } = require('../utils').factories;

class LevelController extends CrudControllerFactoryMethod {
  _service = LevelService;

  constructor() {
    super();
  }

  async countLevels(_, res, next) {
    try {
      res.send(String(await LevelService.countLevels()));
    } catch (error) {
      next(error);
    }
  }

  async getPrevLevel({ body }, res, next) {
    try {
      const { currLevel } = body;

      res.send(await LevelService.getPrevLevel(currLevel));
    } catch (e) {
      next(e);
    }
  }

  async getNextLevel({ body }, res, next) {
    try {
      const { currLevel } = body;

      res.send(await LevelService.getNextLevel(currLevel));
    } catch (e) {
      next(e);
    }
  }
}

module.exports = new LevelController();
