const { CrudControllerFactoryMethod } = require('../utils').factories;
const { MissionService } = require('../services');

class MissionController extends CrudControllerFactoryMethod {
  _service = MissionService;

  constructor() {
    super();
  }

  async getCurrentMission(_, res, next) {
    try {
      res.send(await MissionService.getCurrentMission());
    } catch (e) {
      next(e);
    }
  }

  async resetProgress({ body }, res, next) {
    try {
      const { comment } = body;

      res.send(await MissionService.resetProgress(comment));
    } catch (e) {
      next(e);
    }
  }

  async checkAnswer({ body, params }, res, next) {
    try {
      const { id: missionId } = params;
      const { street, houseNumber } = body;

      res.send(
        await MissionService.checkAnswer(missionId, street, houseNumber)
      );
    } catch (e) {
      next(e);
    }
  }

  async passWithSecretCode({ params, body }, res, next) {
    try {
      const { id: missionId } = params;
      const { secretCode } = body;

      res.send(await MissionService.passWithSecretCode(missionId, secretCode));
    } catch (e) {
      next(e);
    }
  }

  async getAlternativeMission({ params }, res, next) {
    try {
      const { id: missionId } = params;

      res.send(
        await MissionService.getAlternativeForMissionWithMissionID(missionId)
      );
    } catch (error) {
      next(error);
    }
  }
}

module.exports = new MissionController();
