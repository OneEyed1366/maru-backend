const { CrudControllerFactoryMethod } = require('../utils/factories');
const { TransactionService } = require('../services');

class TransactionController extends CrudControllerFactoryMethod {
  _service = TransactionService;

  constructor() {
    super();
  }

  async getBalance(_, res) {
    try {
      res.status(200).send(String(await TransactionService.getBalance()));
    } catch (error) {
      res.status(200).send('0');
    }
  }
}

module.exports = new TransactionController();
