const AnswerController = require('./answer.controller');
const BirdController = require('./bird.controller');
const MissionController = require('./mission.controller');
const LevelController = require('./level.controller');
const TransactionController = require('./transaction.controller');
const SuggestionController = require('./suggestions.controller');

module.exports = {
  AnswerController,
  BirdController,
  MissionController,
  LevelController,
  TransactionController,
  SuggestionController,
};
