const { BirdService } = require('../services');
const { CrudControllerFactoryMethod } = require('../utils').factories;

class BirdController extends CrudControllerFactoryMethod {
  _service = BirdService;

  constructor() {
    super();
  }

  async countSavedBirds(_, res, next) {
    try {
      res.send(String(await BirdService.countSavedBirds()));
    } catch (error) {
      next(error);
    }
  }
}

module.exports = new BirdController();
