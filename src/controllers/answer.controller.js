const { AnswerService } = require('../services');
const { CrudControllerFactoryMethod } = require('../utils').factories;

class AnswerController extends CrudControllerFactoryMethod {
  _service = AnswerService;

  constructor() {
    super();
  }

  async checkAnswer({ body }, res) {
    try {
      const { answer } = body;

      res.send(AnswerService.check(answer));
    } catch (error) {
      res.send(String(0));
    }
  }
}

module.exports = new AnswerController();
