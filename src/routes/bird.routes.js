const { constructRoutePath } = require('../utils');
const { BirdController } = require('../controllers');

module.exports = function (prefix, app) {
  app.get(
    constructRoutePath(prefix, 'count/saved'),
    BirdController.countSavedBirds
  );
};
