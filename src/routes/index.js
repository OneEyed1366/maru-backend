const answers = require('./answers.route');
const birds = require('./bird.routes');
const mission = require('./mission.routes.js');
const level = require('./level.routes');
const transaction = require('./transaction.routes');
const suggestions = require('./suggestions.route');

module.exports = function (app) {
  answers('answers', app);
  birds('birds', app);
  mission('missions', app);
  level('levels', app);
  transaction('coins', app);
  suggestions('suggestions', app);
};
