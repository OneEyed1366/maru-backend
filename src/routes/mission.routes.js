const { constructRoutePath } = require('../utils');
const { MissionController } = require('../controllers');

module.exports = function (prefix, app) {
  app.get(
    constructRoutePath(prefix, 'current'),
    MissionController.getCurrentMission
  );

  app.get(
    constructRoutePath(prefix, 'alternative/:id'),
    MissionController.getAlternativeMission
  );

  app.post(
    constructRoutePath(prefix, 'reset'),
    MissionController.resetProgress
  );

  app.post(
    constructRoutePath(prefix, 'check/:id'),
    MissionController.checkAnswer
  );

  app.post(
    constructRoutePath(prefix, 'secret-code/:id'),
    MissionController.passWithSecretCode
  );
};
