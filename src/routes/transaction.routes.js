const { constructRoutePath } = require("../utils");
const { TransactionController } = require("../controllers");

module.exports = function (prefix, app) {
  app.get(constructRoutePath(prefix), TransactionController.getBalance);
};
