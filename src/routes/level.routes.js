const { LevelController } = require('../controllers');
const { constructRoutePath } = require('../utils');

module.exports = function (prefix, app) {
  app.get(constructRoutePath(prefix, 'count'), LevelController.countLevels);

  app.post(constructRoutePath(prefix, 'next'), LevelController.getNextLevel);

  app.post(constructRoutePath(prefix, 'prev'), LevelController.getPrevLevel);
};
