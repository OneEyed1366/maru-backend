const { constructRoutePath } = require("../utils");
const { AnswerController } = require("../controllers");

module.exports = function (prefix, app) {
  app.post(constructRoutePath(prefix, "check"), AnswerController.checkAnswer);
};
