const { constructRoutePath } = require('../utils');
const { SuggestionController } = require('../controllers');

module.exports = function (prefix, app) {
  app.get(
    constructRoutePath(prefix, ':id'),
    SuggestionController.getSuggestionCost
  );

  app.post(
    constructRoutePath(prefix, 'buy'),
    SuggestionController.buySuggestionWithMissionId
  );
};
