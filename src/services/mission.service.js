const { mission, bird, level, suggestion } = require('../models');
const { CrudServiceFactoryMethod } = require('../utils/factories');
const { TRANSACTION_TYPES_ENUM } = require('../utils/enums.util');
const LevelService = require('./level.service');
const TransactionService = require('./transaction.service');
const BirdService = require('./bird.service');
const SuggestionService = require('./suggestion.service');

class MissionService extends CrudServiceFactoryMethod {
  _model = mission;
  _serviceName = 'MissionService';

  constructor() {
    super();
  }

  async resetProgress() {
    let _transaction, _missions, _birds, _suggestions;
    const comment = 'Missions, Reset Progress';

    try {
      _suggestions = await suggestion.Schema.updateMany(
        {
          isWasUsed: true,
        },
        {
          $set: {
            isWasUsed: false,
          },
        }
      );

      _birds = await bird.Schema.updateMany(
        {
          $or: [{ isSaved: true }, { isDead: true }],
        },
        {
          $set: {
            isSaved: false,
            isDead: false,
          },
        }
      );

      _missions = await this._model.Schema.updateMany(
        { isCompleted: true },
        {
          $set: {
            isCompleted: false,
          },
        }
      );

      await level.Schema.findOneAndUpdate(
        { 'level.value': 1 },
        {
          $set: {
            'level.mission.isActive': true,
          },
        }
      );

      _transaction = await TransactionService.reset(comment);

      return this.getCurrentMission();
    } catch (error) {
      if (_suggestions) {
        await Promise.all(
          _suggestions.map((suggestionToRevert) =>
            suggestion.Schema.findByIdAndUpdate(suggestionToRevert._id, {
              $set: {
                isWasUsed: false,
              },
            })
          )
        );
      }

      if (_transaction) {
        await TransactionService.create({
          comment: `REVERT ${comment}`,
          amount: -_transaction.amount,
          type: TRANSACTION_TYPES_ENUM.ADD,
        });
      }

      if (_birds) {
        await Promise.all(
          _birds.map((bird) =>
            bird.Schema.findByIdAndUpdate(bird._id, {
              isSaved: !bird.isSaved,
              isDead: !bird.isDead,
            })
          )
        );
      }

      if (_missions) {
        await Promise.all(
          _missions.map(
            (mission) => this._model.Schema.findByIdAndUpdate(mission._id),
            { $set: { isCompleted: true } }
          )
        );
      }

      throw new Error(`${this._baseErrorMsg} resetProgress -> ${error}`);
    }
  }

  async getCurrentMission() {
    try {
      const _result = await this._model.Schema.findOne({
        isActive: true,
      }).populate({
        path: 'level alternativeMission bird question answer',
        populate: {
          path: 'image successfulImage answer bird level question',
        },
      });

      const suggestions = await SuggestionService.checkBuyedSuggestions(
        _result._id
      );

      _result.suggestions = suggestions;
      return _result;
    } catch (error) {
      throw new Error(`${this._baseErrorMsg} getCurrentMission -> ${error}`);
    }
  }

  async checkAnswer(missionId, street, houseNumber) {
    let _transaction, _mission, _nextMission;
    let success = false;
    const comment = `MissionService, checkAnswer (missionId: ${missionId}, answer: ${street},${houseNumber})`;

    try {
      if (!missionId) throw new Error('MissionID does not provided!');
      if (!street || !houseNumber) throw new Error('Answer does not provided!');

      _mission = await this._model.Schema.findById(missionId).populate(
        'answer level bird'
      );

      if (!_mission)
        throw new Error(`Mission with MissionID ${missionId} does not found!`);

      if (
        _mission.answer.value.toLowerCase().trim() ===
        `${street},${houseNumber}`.toLowerCase().trim()
      ) {
        _transaction = await TransactionService.create({
          comment,
          amount: _mission.answer.cost,
          type: TRANSACTION_TYPES_ENUM.ADD,
        });

        await BirdService.updateById(_mission.bird._id, {
          isSaved: true,
        });

        success = true;
      }

      _mission.isCompleted = true;
      _mission.isActive = false;
      _mission.save();

      _nextMission = await LevelService.getNextLevel(_mission.level.value);

      return {
        success,
        data: await this._model.Schema.findByIdAndUpdate(
          _nextMission.mission._id,
          {
            $set: {
              isActive: true,
            },
          }
        ),
      };
    } catch (error) {
      if (_nextMission && _nextMission.isActive) {
        await this._model.Schema.findByIdAndUpdate(_nextMission._id, {
          $set: {
            isActive: false,
          },
        });
      }

      if (_transaction) {
        await TransactionService.create({
          amount: -_transaction.amount,
          type: TRANSACTION_TYPES_ENUM.DECREASE,
          comment: `REVERT ${comment}`,
        });
      }

      throw new Error(`${this._baseErrorMsg} checkAnswer -> ${error}`);
    }
  }

  async passWithSecretCode(missionId, secretCode) {
    let _nextMission;

    try {
      if (!missionId) throw new Error('MissionID does not provided!');
      if (!secretCode) throw new Error('SecretCode does not provided!');

      const _mission = await this._model.Schema.findById(missionId).populate(
        'level'
      );
      const isCompleted = _mission.secretCode === secretCode;

      if (isCompleted) {
        await BirdService.updateById(_mission.bird._id, {
          isSaved: true,
          isDead: false,
        });

        _mission.isCompleted = true;
        _mission.isActive = false;
        _mission.save();

        _nextMission = await LevelService.getNextLevel(_mission.level.value);

        return this._model.Schema.findByIdAndUpdate(_nextMission.mission._id, {
          $set: {
            isActive: true,
          },
        });
      } else {
        throw new Error('Invalid SecretCode!');
      }
    } catch (error) {
      throw new Error(`${this._baseErrorMsg} passWithSecretCode -> ${error}`);
    }
  }

  async getAlternativeForMissionWithMissionID(missionId) {
    try {
      const { alternativeMission } = await this._model.Schema.findById(
        missionId
      ).populate({
        path: 'alternativeMission question',
        populate: {
          path: 'question',
        },
      });

      if (!alternativeMission)
        throw new Error('Alternative Mission was not found!');

      return alternativeMission;
    } catch (error) {
      throw new Error(
        `${this._baseErrorMsg} getAlternativeForMissionWithMissionID -> ${error}`
      );
    }
  }
}

module.exports = new MissionService();
