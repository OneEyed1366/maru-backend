const { bird } = require('../models');
const { CrudServiceFactoryMethod } = require('../utils/factories');

class BirdService extends CrudServiceFactoryMethod {
  _model = bird;
  _serviceName = 'BirdService';

  constructor() {
    super();
  }

  async countSavedBirds() {
    try {
      return this._model.Schema.countDocuments({
        isSaved: true,
      });
    } catch (e) {
      throw new Error(`${this._baseErrorMsg} countSavedBirds -> ${e}`);
    }
  }
}

module.exports = new BirdService();
