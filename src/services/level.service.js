const { level } = require('../models');
const { CrudServiceFactoryMethod } = require('../utils/factories');

class LevelService extends CrudServiceFactoryMethod {
  _model = level;
  _serviceName = 'LevelService';

  constructor() {
    super();
  }

  async countLevels() {
    try {
      return this._model.Schema.countDocuments();
    } catch (error) {
      throw new Error(`${this._baseErrorMsg} countLevels -> ${error}`);
    }
  }

  async getPrevLevel(currLevel) {
    try {
      return this._model.Schema.findOne({ value: currLevel - 1 }).populate(
        'mission'
      );
    } catch (error) {
      throw new Error(`${this._baseErrorMsg} getNextLevel -> ${error}`);
    }
  }

  async getNextLevel(currLevel) {
    try {
      return this._model.Schema.findOne({ value: currLevel + 1 }).populate(
        'mission'
      );
    } catch (error) {
      throw new Error(`${this._baseErrorMsg} getNextLevel -> ${error}`);
    }
  }
}

module.exports = new LevelService();
