const { answer } = require("../models");
const { TRANSACTION_TYPES_ENUM } = require("../utils/enums.util");
const CrudFactoryMethod = require("../utils/factories/crud.service-factory");
const TransactionService = require("../services/transaction.service");

class AnswerService extends CrudFactoryMethod {
  _model = answer;
  _serviceName = "AnswerService";

  constructor() {
    super();
  }

  /**
   * @param {string} answer
   *
   * @returns {Promise<number>} Размер вознаграждения за правильный ответ
   * */
  async check(answer) {
    let _transaction;

    try {
      const _answer = this._model.Schema.findOne({ value: answer });

      if (!_answer)
        throw new Error(
          `${this._baseErrorMsg} check -> Such answer does not exist`
        );

      _transaction = TransactionService.create({
        amount: _answer.cost,
        type: TRANSACTION_TYPES_ENUM.ADD,
        comment: `checkAnswer "${answer}"`,
      });

      return _answer.cost;
    } catch (error) {
      if (_transaction) {
        await TransactionService.create({
          amount: -_transaction.amount,
          type: TRANSACTION_TYPES_ENUM.DECREASE,
          comment: `REVERT checkAnswer "${answer}"`,
        });
      }

      throw new Error(`${this._baseErrorMsg} postAnswer -> ${error}`);
    }
  }
}

module.exports = new AnswerService();
