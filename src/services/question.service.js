const { CrudServiceFactoryMethod } = require('../utils/factories');
const { question } = require('../models');

class QuestionService extends CrudServiceFactoryMethod {
  _model = question;
  _serviceName = 'QuestionService';

  constructor() {
    super();
  }
}

module.exports = new QuestionService();
