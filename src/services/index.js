const AnswerService = require('./answer.service');
const BirdService = require('./bird.service');
const TransactionService = require('./transaction.service');
const LevelService = require('./level.service');
const MissionService = require('./mission.service');
const SuggestionService = require('./suggestion.service');
const QuestionService = require('./question.service');

module.exports = {
  AnswerService,
  BirdService,
  TransactionService,
  LevelService,
  MissionService,
  SuggestionService,
  QuestionService,
};
