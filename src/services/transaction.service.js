const { transaction } = require('../models');
const { CrudServiceFactoryMethod } = require('../utils/factories');
const { TRANSACTION_TYPES_ENUM } = require('../utils/enums.util');

class TransactionService extends CrudServiceFactoryMethod {
  _model = transaction;
  _serviceName = 'TransactionService';

  constructor() {
    super();
  }

  async reset(comment) {
    let _transaction;

    try {
      const amount = -(await this.getBalance());

      _transaction = this._model.Schema.create({
        comment,
        amount,
        type: TRANSACTION_TYPES_ENUM.DECREASE,
      });

      return _transaction;
    } catch (error) {
      if (_transaction) {
        await this._model.Schema.create({
          comment: `REVERT ${comment}`,
          amount: -_transaction.amount,
          type: TRANSACTION_TYPES_ENUM.ADD,
        });
      }
      throw new Error(`${this._baseErrorMsg} reset -> ${error}`);
    }
  }

  async safeCreate(data) {
    try {
      const _balance = await this.getBalance();

      if (_balance - Math.abs(data.amount) < 0)
        throw new Error(`Not enough coins!`);

      return this._model.Schema.create(data);
    } catch (error) {
      throw new Error(`${this._baseErrorMsg} create -> ${error}`);
    }
  }
  /**
   * @returns {Promise<number>}
   * */
  async getBalance() {
    try {
      const _result = await this._model.Schema.aggregate([
        { $match: { amount: { $exists: true } } },
        { $group: { _id: null, amount: { $sum: '$amount' } } },
      ]);

      return _result.length ? _result[0].amount : 0;
    } catch (error) {
      throw new Error(`${this._baseErrorMsg} getBalance -> ${error}`);
    }
  }
}

module.exports = new TransactionService();
