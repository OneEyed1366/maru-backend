const { suggestion } = require('../models');
const { TRANSACTION_TYPES_ENUM } = require('../utils/enums.util');
const { CrudServiceFactoryMethod } = require('../utils/factories');
const TransactionService = require('./transaction.service');
const MissionService = require('./mission.service');

class SuggestionService extends CrudServiceFactoryMethod {
  _model = suggestion;
  _serviceName = 'SuggestionService';

  constructor() {
    super();
  }

  async checkBuyedSuggestions(missionId) {
    try {
      const _result = await this._model.Schema.find({
        mission: missionId,
        isWasUsed: true,
      });
      return _result;
    } catch (error) {
      throw new Error(
        `${this._baseErrorMsg} checkBuyedSuggestions -> ${error}`
      );
    }
  }

  /**
   * @param {string} missionId
   * @returns {Promise<number>}
   * */
  async getSuggestionCost(missionId) {
    if (!missionId) throw new Error('MissionID is not provided!');

    const _notUsedSuggestions = await this._model.Schema.find({
      mission: missionId,
      isWasUsed: false,
    });

    if (_notUsedSuggestions.length < 1) return 0;

    return _notUsedSuggestions[0].cost;
  }

  /**
   * @param {string} missionId
   * @returns {Promise<{value: *, suggestionField: *}>}
   * */
  async buySuggestionWithMissionId(missionId) {
    let _transaction, _suggestion;
    const comment = `buySuggestionWithMissionId #${missionId}`;

    try {
      if (!missionId) throw new Error(`MissionID is not provided!`);

      const _notUsedSuggestions = await this._model.Schema.find({
        mission: missionId,
        isWasUsed: false,
      });

      if (_notUsedSuggestions.length < 1)
        throw new Error('All suggestions used!');

      const { _id, value, field, cost: amount } = _notUsedSuggestions[0];

      _transaction = await TransactionService.safeCreate({
        comment,
        amount: -amount,
        type: TRANSACTION_TYPES_ENUM.DECREASE,
      });

      _suggestion = await this.updateById(_id, {
        isWasUsed: true,
      });

      return { value, field, cost: amount };
    } catch (error) {
      if (_suggestion) {
        await this.updateById(_suggestion._id, {
          isWasUsed: false,
        });
      }

      if (_transaction) {
        await TransactionService.create({
          amount: -_transaction.amount,
          type: TRANSACTION_TYPES_ENUM.ADD,
          comment: `REVERT ${comment}`,
        });
      }

      throw new Error(
        `${this._baseErrorMsg} buySuggestionWithLevelId -> ${error}`
      );
    }
  }
}

module.exports = new SuggestionService();
