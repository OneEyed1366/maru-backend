#!/bin/bash

sudo apt-get remove certbot -y
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot
sudo certbot --nginx --force-renewal --email psevdoproger@gmail.com --agree-tos
sudo certbot renew --dry-run