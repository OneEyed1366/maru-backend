#!/bin/bash

sudo rm /etc/nginx/nginx.conf
sudo cp /root/nginx.conf /etc/nginx/nginx.conf
sudo systemctl restart nginx
sudo systemctl status nginx
