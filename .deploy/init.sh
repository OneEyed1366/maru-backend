#!/bin/bash

ACCESS_TOKEN_FRONTEND=glpat-C2MtUNWh19b5zso13UU6
ACCESS_TOKEN_BACKEND=glpat-wxJmedkDRWKtxHmqnr-P
USER=MARU_GAME_BOT

# echo "Docker -> installing..."
# sudo apt update
# sudo apt install apt-transport-https ca-certificates curl software-properties-common -y
# curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
# sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
# sudo apt update
# apt-cache policy docker-ce
# sudo apt install docker-ce -y
# echo "Docker -> installed"
# echo "Docker-compose -> installing..."
# sudo apt-get update
# curl -SL https://github.com/docker/compose/releases/download/v2.14.0/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose
# sudo chmod +x /usr/local/bin/docker-compose
# echo "Docker-compose -> installed"
# echo "NodeJS -> Installing..."
# curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
# source ~/.bashrc
# nvm install 16.19.0
# echo "NodeJS -> Installed"
# echo "Yarn -> installing..."
# npm i -G yarn
# echo "Yarn -> Installed"
# echo "Git"
# sudo apt install git -y
# git config --global user.name $USER
# git config --global user.email "maru-game.mail.ru"
# git clone "https://${USER}:${ACCESS_TOKEN_FRONTEND}@gitlab.com/eight_in_the_evening/marusya_and_popugs.git" client
# cd /root/client && git pull && git switch dev && yarn && cd /root
# git clone "https://${USER}:${ACCESS_TOKEN_BACKEND}@gitlab.com/OneEyed1366/maru-backend.git" server
# cd /root/server && git pull && yarn && cd /root
# echo "Git -> Completed!"
echo "BD Setup"
cd /root/server && docker-compose --env-file ./.env -p maru-prod --file .deploy/docker-compose.yml up -d --remove-orphans && cd /root
echo "BD Setup -> Completed!"
echo "Nginx -> installing..."
sudo apt install nginx -y
sudo ufw allow 'Nginx FULL'
echo "Nginx -> installed"
echo "Nginx -> configuring..."
sudo rm /etc/nginx/nginx.conf
sudo cp /root/nginx.conf /etc/nginx/nginx.conf
sudo systemctl restart nginx
sudo systemctl status nginx
echo "Nginx -> configured"
# echo "CertBot -> installing..."
sudo apt install snapd -y
sudo snap install core
sudo snap refresh core
sudo apt-get remove certbot -y
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot
sudo certbot --nginx --force-renewal --email psevdoproger@gmail.com --agree-tos
sudo certbot renew --dry-run
echo "CertBot -> installed"
echo "All set"
