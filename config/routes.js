'use strict';
const routes = require('../src/routes');
/**
 * Expose routes
 * @param app
 * @param passport
 */
module.exports = function (app) {
  routes(app);

  /**
   * Error handling
   */
  app.use(function (req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;

    next(err);
  });

  if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
      res.status(err.status || 500);
      res.send({
        message: err.message,
        error: err,
      });
    });
  }

  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.send({
      message: err.message,
      error: {},
    });
  });
};
