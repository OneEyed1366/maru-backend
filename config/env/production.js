"use strict";

/**
 * Expose
 */

const { MONGODB_PORT, MONGODB_USER, MONGODB_PASSWORD, MONGODB_DB_NAME } =
  process.env;

module.exports = {
  db: `mongodb://${MONGODB_USER}:${MONGODB_PASSWORD}@127.0.0.1:${MONGODB_PORT}/${MONGODB_DB_NAME}?authSource=admin`,
};
